package com.henderson_group.hws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Migillator2Application {

	public static void main(String[] args) {
		SpringApplication.run(Migillator2Application.class, args);
	}
}
